﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using YodelingKidAPI.Models;

namespace YodelingKidAPI
{
    public class SoundRecords : ISoundbaseApi
    {
        string defaultPath = @"İsyanqar Duygular Records Content\";
        [WebInvoke(Method = "GET")]
        public Stream GetSound(int id)
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "audio/mpeg";
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(defaultPath + id.ToString() + ".mp3"));
            return ms;
        }

        [WebInvoke(Method = "GET")]
        public Stream GetPicture(int id)
        {
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(defaultPath + id.ToString() + ".jpg"));
            return ms;
        }

        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET")]
        public List<Sound> List()
        {
            using (YodelbaseEntities yb = new YodelbaseEntities())
            {
                return yb.Sounds.ToList();
            }
        }
    }
}
