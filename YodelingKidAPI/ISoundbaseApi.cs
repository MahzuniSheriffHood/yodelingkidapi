﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization;
using System.ServiceModel;
using YodelingKidAPI.Models;

namespace YodelingKidAPI
{
    [ServiceContract]
    public interface ISoundbaseApi
    {
        [OperationContract]
        Stream GetSound(int id);

        [OperationContract]
        List<Sound> List();

        [OperationContract]
        Stream GetPicture(int id);
    }
}